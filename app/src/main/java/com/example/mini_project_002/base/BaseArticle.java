package com.example.mini_project_002.base;

public class BaseArticle {
    private String username;
    private String postTime;
    private String description;
    private String status;
    private String bookmark;

    public BaseArticle() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPostTime() {
        return postTime;
    }

    public void setPostTime(String postTime) {
        this.postTime = postTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBookmark() {
        return bookmark;
    }

    public void setBookmark(String bookmark) {
        this.bookmark = bookmark;
    }

    @Override
    public String toString() {
        return "Post{" +
                "username='" + username + '\'' +
                ", postTime='" + postTime + '\'' +
                ", description='" + description + '\'' +
                ", status='" + status + '\'' +
                ", bookmark='" + bookmark + '\'' +
                '}';
    }
}
