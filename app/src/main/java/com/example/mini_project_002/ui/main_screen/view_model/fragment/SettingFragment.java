package com.example.mini_project_002.ui.main_screen.view_model.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.mini_project_002.R;
import com.example.mini_project_002.data.adapter.ArticleAdapter;
import com.example.mini_project_002.data.model.UserResponseBody;
import com.example.mini_project_002.data.model.model.Article;
import com.example.mini_project_002.data.model.request.BookMarkRequest;
import com.example.mini_project_002.data.model.response.ArticleResponse;
import com.example.mini_project_002.databinding.FragmentSettingBinding;
import com.example.mini_project_002.ui.main_screen.ProfileUpdateActivity;
import com.example.mini_project_002.ui.main_screen.view_model.ArticleViewModel;
import com.example.mini_project_002.ui.main_screen.view_model.BookmarkViewModel;
import com.example.mini_project_002.ui.main_screen.view_model.SettingViewModel;

import java.util.ArrayList;
import java.util.List;

public class SettingFragment extends Fragment {
    View view;
    FragmentSettingBinding binding;
    Intent intent;
    String id,ownerid;
    SharedPreferences preferences;
    SettingViewModel settingViewModel;
    ArticleViewModel articleViewModel;
    BookmarkViewModel bookmarkViewModel;
    private ArrayList<Article> articles;
    private ArticleAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false);
        view = binding.getRoot();
        return view;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        articleViewModel = new ViewModelProvider(requireActivity(), new ViewModelProvider.AndroidViewModelFactory(getActivity().getApplication())).get(ArticleViewModel.class);

        binding.btnEdit.setOnClickListener(v -> {
            intent = new Intent(getActivity(), ProfileUpdateActivity.class);
            startActivity(intent);
        });
        preferences = getActivity().getSharedPreferences("User", Context.MODE_PRIVATE);
        id = preferences.getString("UserId", "");
        settingViewModel = new ViewModelProvider(requireActivity() ,new ViewModelProvider.AndroidViewModelFactory(getActivity().getApplication())).get(SettingViewModel.class);
        getUser(id);
        getOwnerArticle();


    }

    private void getOwnerArticle() {
        articleViewModel.getAllArticles("1", "500");
        articleViewModel.articleLiveData.observe(getViewLifecycleOwner(), new Observer<ArticleResponse>() {
            @Override
            public void onChanged(ArticleResponse response) {
                Log.d(">>", "onChanged: response" +response.getPayload().get(0).getId());
               int payloadSize = response.getPayload().size();
                List<String> ownerIds = new ArrayList<>();
                List<Article> filteredArticles = new ArrayList<>();
               for(int i = 0; i<payloadSize; i ++){
                   ownerid = response.getPayload().get(i).getTeacher().getId(); // Extract ownerid
                   Log.d(">> id", "onChanged: " + ownerid);
                   ownerIds.add(ownerid); // Add ownerid to the list
                   if (ownerid.equals(id)) {
                       filteredArticles.add(response.getPayload().get(i));
                   } else {

                   }
               }
                // Update the RecyclerView with the filtered articles
                articles = new ArrayList<>();
                articles.addAll(filteredArticles);
                adapter = new ArticleAdapter(getContext(), articles, position -> {
                    Article articlesList = articles.get(position);
                    bookmarkViewModel= new ViewModelProvider(getActivity(), new ViewModelProvider
                            .AndroidViewModelFactory(getActivity().getApplication()))
                            .get(BookmarkViewModel.class);
                    bookmarkViewModel.addBookMark(id, new BookMarkRequest(articlesList.getId()));
                });
                binding.settingRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                binding.settingRecyclerView.setAdapter(adapter);

            }
        });
    }

    public void getUser(String id){
        settingViewModel.UserProfile(id);
        settingViewModel.user.observe(getViewLifecycleOwner(), new Observer<UserResponseBody>() {
            @Override
            public void onChanged(UserResponseBody userResponseBody) {
                Glide.with(getContext())
                        .load(userResponseBody.getPayload().getProfile())
                        .placeholder(R.drawable.profile2_post)
                        .into(binding.imgProfile);
                binding.txtUsername.setText(userResponseBody.getPayload().getUsername());
                binding.txtEmail.setText(userResponseBody.getPayload().getEmail());
            }
        });

   }

}