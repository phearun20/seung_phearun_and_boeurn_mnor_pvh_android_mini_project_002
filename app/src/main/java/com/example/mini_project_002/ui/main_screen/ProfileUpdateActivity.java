package com.example.mini_project_002.ui.main_screen;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.example.mini_project_002.R;
import com.example.mini_project_002.base.BaseActivity;
import com.example.mini_project_002.data.model.ImageResponse;
import com.example.mini_project_002.data.model.UserResponseBody;
import com.example.mini_project_002.data.model.request.UserRequest;
import com.example.mini_project_002.databinding.ActivityProfileUpdateBinding;
import com.example.mini_project_002.ui.main_screen.view_model.LoginViewModel;
import com.example.mini_project_002.ui.main_screen.view_model.UpdateUserViewModel;
import com.example.mini_project_002.ui.main_screen.view_model.UploadViewModel;
import com.example.mini_project_002.ui.main_screen.view_model.fragment.SettingFragment;

import java.io.File;
import java.util.Collections;

public class ProfileUpdateActivity extends BaseActivity<ActivityProfileUpdateBinding> {
    String[] role = { "TEACHER", "READER"};
    UpdateUserViewModel updateUserViewModel;
    UploadViewModel uploadViewModel;
    String getRole = "TEACHER";
    String imageUrl ="http://8.219.139.67:6654/api/v1/files/view/64a00910-5247-4e9c-ab68-8b203bd19b7a.png";
    String userName,email,phone,userId;
    SharedPreferences preferences;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uploadViewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(getApplication())).get(UploadViewModel.class);
        updateUserViewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(getApplication())).get(UpdateUserViewModel.class);
        binding.selectImage.setOnClickListener(v->{
            selectImageFromGallery();
        });
        preferences = getSharedPreferences("User",MODE_PRIVATE);
        userId = preferences.getString("UserId", "");
        Log.d("TAG", "onCreate: userId"+userId);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, role);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.role.setAdapter(adapter);
        binding.role.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                getRole = binding.role.getSelectedItem().toString();
                Log.e("TAG", getRole);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // Handle the case where nothing is selected, if needed
            }
        });
        binding.createAcc.setOnClickListener(v -> {
            userName = binding.userName.getText().toString();
            email =binding.email.getText().toString();
            phone =binding.phone.getText().toString();

            if (TextUtils.isEmpty(userName)) {
                binding.userName.setError("Username is Required");
                return;
            }
            if (TextUtils.isEmpty(email)) {
                binding.email.setError("Email is Required");
                return;
            } else if (!isValidEmail(email)) {
                binding.email.setError("Invalid Email Format");
                return;
            }
            if (TextUtils.isEmpty(phone)) {
                binding.phone.setError("Phone is Required");
                return;
            }
            updateUser();
        });

    }
    private boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    private void updateUser() {

        updateUserViewModel.updateUser(new UserRequest(userName,
                email,imageUrl,phone, Collections.singletonList(getRole)
        ),userId);
        updateUserViewModel.user.observe(this, new Observer<UserResponseBody>() {

                    @Override
                    public void onChanged(UserResponseBody userResponseBody) {
                        Log.d(">>", "id: " +userResponseBody.getPayload().getId());

                        if(userResponseBody.getPayload().getId() != null){
//                            SharedPreferences.Editor editor = preferences.edit();
//                            editor.putString("UserId",userResponseBody.getPayload().getId().toString());
//                            editor.commit();
                            binding.userName.setText("");
                            binding.email.setText("");
                            binding.phone.setText("");
                            intent = new Intent(ProfileUpdateActivity.this, MainPageActivity.class);
                            intent.putExtra("fragmentSetting", "setting");
                            startActivity(intent);
                        }
                    }

                }

        );
    }
    private void selectImageFromGallery() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_MEDIA_IMAGES) == PackageManager.PERMISSION_GRANTED) {
            pickImageLauncher.launch("image/*");
        } else {
            requestPermissionLauncher.launch(Manifest.permission.READ_MEDIA_IMAGES);
        }
    }
    private ActivityResultLauncher<String> requestPermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), new ActivityResultCallback<Boolean>() {
        @Override
        public void onActivityResult(Boolean result) {
            if (result.equals(true)) {
                pickImageLauncher.launch("image/*");
            } else {
                Toast.makeText(ProfileUpdateActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    });
    private ActivityResultLauncher<String> pickImageLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), uri -> {
        String selectedFilePath = com.example.mini_project_002.ui.utils.RealPathUtil.getRealPath(this,uri );
        binding.selectImage.setImageURI(uri);

        File file = new File(selectedFilePath);
        Log.e("TAG>>", ": "+file);
        formDataConverter(file);

    });
    private void formDataConverter(File file) {
        uploadViewModel.uploadImage(file);
        uploadViewModel.image.observe(this, new Observer<ImageResponse>() {
            @Override
            public void onChanged(ImageResponse imageResponse) {
                Log.d("imageResponse>>", "onChanged: "+ imageResponse.getImageurl());
                imageUrl = imageResponse.getImageurl().get(0).toString();
            }
        });

    }

    @Override
    public int layout() {
        return R.layout.activity_profile_update;
    }
}