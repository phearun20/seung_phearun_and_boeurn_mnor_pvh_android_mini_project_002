package com.example.mini_project_002.ui.main_screen.view_model;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mini_project_002.data.model.UserResponseBody;
import com.example.mini_project_002.data.model.request.UserRequest;
import com.example.mini_project_002.data.repo.UserRepository;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class UpdateUserViewModel extends AndroidViewModel {
 UserRepository userRepository;
    MutableLiveData<UserResponseBody> _user = new MutableLiveData<UserResponseBody>();
    public LiveData<UserResponseBody> user = _user;

    public UpdateUserViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository();
    }
    public void updateUser(UserRequest userRequest,String userId){
    userRepository.updateUser(userRequest,userId)
        .subscribe(new Observer<UserResponseBody>() {
            @Override
            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                Log.d("TAG", "onSubscribe: ");
            }

            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull UserResponseBody userResponseBody) {
                Log.d(">>>", "onNext: " + userResponseBody);
                _user.postValue(userResponseBody);
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                Log.e("TAG", "onError: "+e );
            }

            @Override
            public void onComplete() {
                Log.e("TAG", "onComplete: " );
            }
        });
    }
}


