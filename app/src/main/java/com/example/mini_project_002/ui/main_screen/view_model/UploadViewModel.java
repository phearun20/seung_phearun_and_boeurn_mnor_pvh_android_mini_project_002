package com.example.mini_project_002.ui.main_screen.view_model;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mini_project_002.data.model.ImageResponse;
import com.example.mini_project_002.data.repo.UploadImageRepository;
import com.example.mini_project_002.data.repo.UserRepository;

import java.io.File;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UploadViewModel extends AndroidViewModel {
    UploadImageRepository uploadImageRepository;
    MutableLiveData<ImageResponse> _image = new MutableLiveData<>();
    public LiveData<ImageResponse> image = _image;

    public UploadViewModel(@NonNull Application application) {
        super(application);
        uploadImageRepository = new UploadImageRepository();
    }
    public void uploadImage (File file){
        RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part[] body = new MultipartBody.Part[2];
        body[0] = MultipartBody.Part.createFormData("files", file.getName(), requestBody);

        uploadImageRepository.uploadImage(body)
        .subscribeOn(Schedulers.newThread())
                .subscribe(new Observer<ImageResponse>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.rxjava3.annotations.NonNull ImageResponse imageResponse) {
                        Log.e(">>", "onNext: "+ imageResponse.getImageurl());
                        _image.postValue(imageResponse);

                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
//                        Log.e(">>", "onNext: "+ e.toString());
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

}
