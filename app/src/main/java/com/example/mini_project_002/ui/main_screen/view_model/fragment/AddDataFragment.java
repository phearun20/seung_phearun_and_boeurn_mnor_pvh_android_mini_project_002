package com.example.mini_project_002.ui.main_screen.view_model.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.mini_project_002.R;
import com.example.mini_project_002.data.model.ImageResponse;
import com.example.mini_project_002.data.model.model.Category;
import com.example.mini_project_002.data.model.request.ArticleRequest;
import com.example.mini_project_002.data.model.response.CategoryResponse;
import com.example.mini_project_002.databinding.CustomDialogBinding;
import com.example.mini_project_002.databinding.FragmentAddDataBinding;
import com.example.mini_project_002.databinding.FragmentBookmarkBinding;
import com.example.mini_project_002.databinding.FragmentHomeBinding;
import com.example.mini_project_002.ui.main_screen.LoginActivity;
import com.example.mini_project_002.ui.main_screen.view_model.ArticleViewModel;
import com.example.mini_project_002.ui.main_screen.view_model.CategoryViewModel;
import com.example.mini_project_002.ui.main_screen.view_model.UploadViewModel;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.io.File;
import java.util.Collections;
import java.util.List;

public class AddDataFragment extends Fragment {
    View view;
    FragmentAddDataBinding binding;
    SharedPreferences sharedPreferences;
    UploadViewModel uploadViewModel;
    ArticleViewModel articleViewModel;
    CategoryViewModel categoryViewModel;
    String[] item;
    String description,categories,teacherId,thumbnail;
    String title = "hii";
    boolean isPublished = false;

    String page, size;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_data, container, false);
        view = binding.getRoot();
        uploadViewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(requireActivity().getApplication())).get(UploadViewModel.class);
        articleViewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(requireActivity().getApplication())).get(ArticleViewModel.class);
        categoryViewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(requireActivity().getApplication())).get(CategoryViewModel.class);
        return view;
    }
    private void selectCategory() {
        MaterialAlertDialogBuilder builder = new MaterialAlertDialogBuilder(getContext());
        builder.setTitle("Category")
                .setItems(item, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        categories = item[i];
                        Log.d(">>>", "onClick: categories " +categories);

                    }
                })
                .setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {

                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getCategory(page, size);
        binding.dialoglineaLayout.setOnClickListener(view1 -> {
            selectCategory();
        });
        binding.selectImage.setOnClickListener(v->{
            selectImageFromGallery();
        });
        binding.btnPublishPrivate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Handle the switch state change here
                if (isChecked) {
                    // The switch is ON
                    // Perform actions for the ON state
                    isPublished =true;
                    Log.e("isChecked", "onCheckedChanged: "+ isPublished );
                } else {
                    // The switch is OFF
                    // Perform actions for the OFF state
                    isPublished =false;
                    Log.e("isChecked", "onCheckedChanged: "+ false );
                }
            }
        });

        binding.btnPost.setOnClickListener(v -> {

            description = binding.txtDescription.getText().toString();
            postArticle();
        });
    }

    private void getCategory(String page, String size) {
        categoryViewModel.getAllCategory("1", "10");
        categoryViewModel.categoryLiveData.observe(getViewLifecycleOwner(), new Observer<CategoryResponse>() {
            @Override
            public void onChanged(CategoryResponse categoryResponse) {
                Log.d(">>", "onChanged: categoryResponse" + categoryResponse);
                List<Category> categoryList = categoryResponse.getPayload();

                // Create an array of strings to store the category names
                item = new String[categoryList.size()];
                Log.d(">>", "onChanged: item"+ item);
                for (int i = 0; i < categoryList.size(); i++) {
                    item[i] = categoryList.get(i).getName();
                }
            }
        });
    }

    private void postArticle() {
        sharedPreferences = getActivity().getSharedPreferences("User", Context.MODE_PRIVATE);
         teacherId = sharedPreferences.getString("UserId", "");
        Log.d("TAG", "postArticle: "+teacherId);
        articleViewModel.AddArticle(new ArticleRequest(title,description, Collections.singletonList(categories),teacherId,isPublished,thumbnail));
    }

    private void selectImageFromGallery() {

        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.READ_MEDIA_IMAGES) == PackageManager.PERMISSION_GRANTED) {
            pickImageLauncher.launch("image/*");
        } else {
            requestPermissionLauncher.launch(android.Manifest.permission.READ_MEDIA_IMAGES);
        }
    }
    private ActivityResultLauncher<String> requestPermissionLauncher = registerForActivityResult(new ActivityResultContracts.RequestPermission(), new ActivityResultCallback<Boolean>() {
        @Override
        public void onActivityResult(Boolean result) {
            if (result.equals(true)) {
                pickImageLauncher.launch("image/*");
            } else {
//                Toast.makeText(LoginActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
            }
        }
    });

    private ActivityResultLauncher<String> pickImageLauncher = registerForActivityResult(new ActivityResultContracts.GetContent(), uri -> {
        String selectedFilePath = com.example.mini_project_002.ui.utils.RealPathUtil.getRealPath(getContext(),uri );
        binding.selectImage.setImageURI(uri);

        File file = new File(selectedFilePath);
        formDataConverter(file);

    });


    private void formDataConverter(File file) {
        uploadViewModel.uploadImage(file);
        uploadViewModel.image.observe(this, new Observer<ImageResponse>() {
            @Override
            public void onChanged(ImageResponse imageResponse) {
                Log.d(">>", "onChanged: postatt"+ imageResponse.getImageurl());
                thumbnail = imageResponse.getImageurl().get(0).toString();
            }
        });
    }

}
