package com.example.mini_project_002.ui.main_screen;

import static androidx.databinding.DataBindingUtil.setContentView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.mini_project_002.R;
import com.example.mini_project_002.base.BaseActivity;
import com.example.mini_project_002.databinding.ActivityMainPageBinding;
import com.example.mini_project_002.ui.main_screen.view_model.fragment.AddDataFragment;
import com.example.mini_project_002.ui.main_screen.view_model.fragment.BookmarkFragment;
import com.example.mini_project_002.ui.main_screen.view_model.fragment.HomeFragment;
import com.example.mini_project_002.ui.main_screen.view_model.fragment.NotificationFragment;
import com.example.mini_project_002.ui.main_screen.view_model.fragment.SettingFragment;

public class MainPageActivity extends BaseActivity<ActivityMainPageBinding> {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        fragmentHomeBinding = FragmentHomeBinding.inflate(getLayoutInflater());
//        setContentView(fragmentHomeBinding.getRoot());

        // Check if an extra with the fragment identifier is passed
        String fragmentToOpen = getIntent().getStringExtra("fragmentSetting");
        if (fragmentToOpen != null) {
            if (fragmentToOpen.equals("setting")) {
                replaceFragment(new SettingFragment());
            }
            // Add more conditions for other fragments as needed
        } else {
            // If no extra is provided, default to HomeFragment
            replaceFragment(new HomeFragment());
        }
        binding.bottomNavigationView.setBackground(null);

        binding.bottomNavigationView.setOnItemReselectedListener(item -> {
            int itemId = item.getItemId();
            if (itemId == R.id.home2){
                replaceFragment(new HomeFragment());
            } else if (itemId == R.id.bookmark) {
                replaceFragment(new BookmarkFragment());
            } else if (itemId == R.id.notification) {
                replaceFragment(new NotificationFragment());
            } else if (itemId == R.id.setting){
                replaceFragment(new SettingFragment());
            }
        });

//        MenuItem homeMenuItem = binding.bottomNavigationView.getMenu().findItem(R.id.home2);
//        MenuItem bookmarkMenuItem = binding.bottomNavigationView.getMenu().findItem(R.id.bookmark);
//        MenuItem notificationMenuItem = binding.bottomNavigationView.getMenu().findItem(R.id.notification);
//        MenuItem settingMenuItem = binding.bottomNavigationView.getMenu().findItem(R.id.setting);
//
//        homeMenuItem.setOnMenuItemClickListener(item -> {
//            replaceFragment(new HomeFragment());
//            return true;
//        });
//
//        bookmarkMenuItem.setOnMenuItemClickListener(item -> {
//            replaceFragment(new BookmarkFragment());
//            return true;
//        });
//
//        notificationMenuItem.setOnMenuItemClickListener(item -> {
//            replaceFragment(new NotificationFragment());
//            return true;
//        });
//
//        settingMenuItem.setOnMenuItemClickListener(item -> {
//            replaceFragment(new SettingFragment());
//            return true;
//        });

        binding.btnPost.setOnClickListener(view -> {
            replaceFragment(new AddDataFragment());
            });
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.frame_layout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public int layout() {
        return R.layout.activity_main_page;
    }
}


