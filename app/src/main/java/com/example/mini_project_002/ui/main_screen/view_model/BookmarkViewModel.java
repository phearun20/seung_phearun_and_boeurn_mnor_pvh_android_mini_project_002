package com.example.mini_project_002.ui.main_screen.view_model;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mini_project_002.data.model.model.Bookmark;
import com.example.mini_project_002.data.model.request.BookMarkRequest;
import com.example.mini_project_002.data.model.response.BookmarkResponse;
import com.example.mini_project_002.data.repo.BookmarkRepository;

import java.util.List;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class BookmarkViewModel extends AndroidViewModel {
    BookmarkRepository bookmarkRepository;
    MutableLiveData<List<Bookmark>> mutableLiveData = new MutableLiveData<java.util.List<com.example.mini_project_002.data.model.model.Bookmark>>();
    public LiveData<List<Bookmark>> bookmarkLiveData = mutableLiveData;
    public BookmarkViewModel(@NonNull Application application) {
        super(application);
        bookmarkRepository = new BookmarkRepository();
    }
    public void getAllBookmarkByUserId(String id) {
        bookmarkRepository.getAllBookmarkByUserId(id)
                .subscribe(new Observer<BookmarkResponse>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                        Log.e(">>>", "onSubscribe: " + d);
                    }

                    @Override
                    public void onNext(@io.reactivex.rxjava3.annotations.NonNull BookmarkResponse bookmarkResponse) {
                        mutableLiveData.postValue(bookmarkResponse.getPayload());
                        Log.d(">>>", "onNext: bookmarkResponse" + bookmarkResponse.getPayload());
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        Log.d(">>>", "onError: "+e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
    public  void addBookMark(String userId,BookMarkRequest bookMarkRequest ){

        bookmarkRepository.addBookMark(userId,new BookMarkRequest(bookMarkRequest.getArticleId()) )
                .subscribe(new Observer<Void>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.rxjava3.annotations.NonNull Void unused) {

                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });


    }
}
