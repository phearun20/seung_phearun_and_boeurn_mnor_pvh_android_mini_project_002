package com.example.mini_project_002.ui.main_screen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.mini_project_002.R;
import com.example.mini_project_002.base.BaseActivity;
import com.example.mini_project_002.databinding.ActivityMainBinding;

public class MainActivity extends BaseActivity<ActivityMainBinding> {
String user ;
Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        user ="1";
        if(user.equals("1")){
            intent = new Intent(this,LoginActivity.class);
            startActivity(intent);
        }else{
            intent = new Intent(this,MainPageActivity.class);
            startActivity(intent);

        }
    }


    @Override
    public int layout() {
        return R.layout.activity_main;
    }
}