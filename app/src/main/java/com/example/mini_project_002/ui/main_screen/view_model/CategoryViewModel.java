package com.example.mini_project_002.ui.main_screen.view_model;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mini_project_002.data.model.response.ArticleResponse;
import com.example.mini_project_002.data.model.response.CategoryResponse;
import com.example.mini_project_002.data.repo.ArticleRepository;
import com.example.mini_project_002.data.repo.CategoryRepository;

import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class CategoryViewModel extends AndroidViewModel {
    CategoryRepository categoryRepository;

    MutableLiveData<CategoryResponse> mutableLiveData = new MutableLiveData<>();
    public LiveData<CategoryResponse> categoryLiveData = mutableLiveData;
    public CategoryViewModel(@NonNull Application application) {
        super(application);
        categoryRepository = new CategoryRepository();
    }
    public void getAllCategory(String page,String size){
        categoryRepository.getAllUser(page,size)
                .subscribe(new Observer<CategoryResponse>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@io.reactivex.rxjava3.annotations.NonNull CategoryResponse categoryResponse) {
                        Log.d(">>", "onNext: categoryResponse"+ categoryResponse);
                        mutableLiveData.postValue(categoryResponse);
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
