package com.example.mini_project_002.ui.main_screen.view_model.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.mini_project_002.R;
import com.example.mini_project_002.data.adapter.ArticleAdapter;
import com.example.mini_project_002.data.model.model.Article;
import com.example.mini_project_002.data.model.request.BookMarkRequest;
import com.example.mini_project_002.data.model.response.ArticleResponse;
import com.example.mini_project_002.databinding.FragmentHomeBinding;
import com.example.mini_project_002.ui.main_screen.view_model.ArticleViewModel;
import com.example.mini_project_002.ui.main_screen.view_model.BookmarkViewModel;

import java.util.ArrayList;

public class HomeFragment extends Fragment {
    View view;
    FragmentHomeBinding binding;
    ArticleViewModel articleViewModel;
    private ArrayList<Article> articles;
    private RecyclerView recyclerView;
    private ArticleAdapter adapter;
    String userId;
    SharedPreferences sharedPreferences;
    BookmarkViewModel bookmarkViewModel;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        view = binding.getRoot();
        sharedPreferences = getActivity().getSharedPreferences("User", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("UserId", "");
        articleViewModel = new ViewModelProvider(requireActivity(), new ViewModelProvider.AndroidViewModelFactory(getActivity().getApplication())).get(ArticleViewModel.class);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        articleViewModel.getAllArticles("1", "500");

        articleViewModel.articleLiveData.observe(getViewLifecycleOwner(), new Observer<ArticleResponse>() {
            @Override
            public void onChanged(ArticleResponse response) {
                Log.d(">>", "onChanged: response" +response.getPayload().get(0).getId());
                articles = new ArrayList<>();
                articles.addAll(response.getPayload());

                adapter = new ArticleAdapter(getContext(), articles, position -> {
                    Article articlesList = articles.get(position);
                    bookmarkViewModel= new ViewModelProvider(getActivity(), new ViewModelProvider
                            .AndroidViewModelFactory(getActivity().getApplication()))
                            .get(BookmarkViewModel.class);
                    bookmarkViewModel.addBookMark(userId, new BookMarkRequest(articlesList.getId()));
                });
                binding.homeRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                binding.homeRecyclerView.setAdapter(adapter);
            }
        });
    }


}