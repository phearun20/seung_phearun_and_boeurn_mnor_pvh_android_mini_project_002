package com.example.mini_project_002.ui;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.mini_project_002.R;
import com.example.mini_project_002.base.BaseActivity;
import com.example.mini_project_002.data.model.response.ArticleResponse;
import com.example.mini_project_002.databinding.FragmentHomeBinding;
import com.example.mini_project_002.ui.main_screen.view_model.ArticleViewModel;

public class ArticleActivity extends BaseActivity<FragmentHomeBinding> {
    ArticleViewModel articleViewModel;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        articleViewModel = new ViewModelProvider(this, new ViewModelProvider.AndroidViewModelFactory(getApplication())).get(ArticleViewModel.class);
//
//        articleViewModel.getAllArticles("1","20");

        articleViewModel.articleLiveData.observe(this, new Observer<ArticleResponse>() {

            @Override
            public void onChanged(ArticleResponse articleResponse) {

            }
        });
    }

    @Override
    public int layout() {
        return R.layout.fragment_home;
    }
}
