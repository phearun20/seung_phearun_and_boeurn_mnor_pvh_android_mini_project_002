package com.example.mini_project_002.ui.main_screen.view_model;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mini_project_002.data.model.model.Article;
import com.example.mini_project_002.data.model.request.ArticleRequest;
import com.example.mini_project_002.data.model.response.ArticleResponse;
import com.example.mini_project_002.data.repo.ArticleRepository;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class ArticleViewModel extends AndroidViewModel {
    ArticleRepository articleRepository;
    MutableLiveData<ArticleResponse> mutableLiveData = new MutableLiveData<>();
    public LiveData<ArticleResponse> articleLiveData = mutableLiveData;
     public ArticleViewModel(@NonNull Application application) {
        super(application);
        articleRepository = new ArticleRepository();
    }

    public void getAllArticles(String page, String size) {
         articleRepository.getAllArticles("1", "500")
//                 .subscribeOn(Schedulers.newThread())
                 .subscribe(new Observer<ArticleResponse>() {

                     @Override
                     public void onError(Throwable e) {
                         Log.d(">>", "onError:1233 "+e);

                     }

                     @Override
                     public void onComplete() {
                         Log.e(">>", "onComplete: " );
                     }

                     @Override
                     public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                         Log.e(">>", "onSubscribe: "+d );
                     }

                     @Override
                     public void onNext(ArticleResponse articleResponse) {
                         mutableLiveData.postValue(articleResponse);
                         Log.d(">>", "onNext: articleResponse"+articleResponse.getPayload());
                     }
                 });


    }
    public void AddArticle(ArticleRequest articleRequest){
         articleRepository.addArticle(articleRequest)
                 .subscribe(new Observer<Void>() {
                     @Override
                     public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                         Log.d(">>", "onSubscribe: "+d);
                     }

                     @Override
                     public void onNext(@io.reactivex.rxjava3.annotations.NonNull Void unused) {

                     }

                     @Override
                     public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                         Log.e(">>", "onError: "+e );
                     }

                     @Override
                     public void onComplete() {

                     }
                 });
    }

}
