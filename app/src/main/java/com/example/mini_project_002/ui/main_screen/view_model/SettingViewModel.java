package com.example.mini_project_002.ui.main_screen.view_model;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mini_project_002.data.model.UserResponseBody;
import com.example.mini_project_002.data.repo.UserRepository;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;

public class SettingViewModel extends AndroidViewModel {
    UserRepository userRepository;
    MutableLiveData<UserResponseBody> _user = new MutableLiveData<UserResponseBody>();
    public LiveData<UserResponseBody> user = _user;
    public SettingViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository();
    }
    public void UserProfile(String id){
            userRepository.getOwnerUser(id)
                    .subscribe(new Observer<UserResponseBody>() {
                        @Override
                        public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                            Log.e(">>>", "onSubscribe: "+d);
                        }

                        @Override
                        public void onNext(@io.reactivex.rxjava3.annotations.NonNull UserResponseBody userResponseBody) {
                            Log.e(">>>", "onNext: userResponseBody"+ userResponseBody );
                            _user.postValue(userResponseBody);
                        }

                        @Override
                        public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                            Log.e(">>>", "onError: "+ e );
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
    }
}
