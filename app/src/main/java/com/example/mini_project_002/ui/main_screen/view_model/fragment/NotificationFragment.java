package com.example.mini_project_002.ui.main_screen.view_model.fragment;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mini_project_002.R;
import com.example.mini_project_002.databinding.FragmentBookmarkBinding;
import com.example.mini_project_002.databinding.FragmentNotificationBinding;

public class NotificationFragment extends Fragment {
    View view;
    FragmentNotificationBinding binding;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
        view = binding.getRoot();

        return view;
    }
}