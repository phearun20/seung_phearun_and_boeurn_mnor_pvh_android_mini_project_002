package com.example.mini_project_002.ui.main_screen.view_model;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mini_project_002.data.model.UserResponseBody;
import com.example.mini_project_002.data.model.request.UserRequest;
import com.example.mini_project_002.data.repo.UserRepository;

import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;


public class LoginViewModel extends AndroidViewModel {
    UserRepository userRepository;
    MutableLiveData<UserResponseBody> _user = new MutableLiveData<UserResponseBody>();
    public LiveData<UserResponseBody> user = _user;
    public LoginViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository();
    }
    public void AddNewUser(UserRequest userRequest) {
        userRepository.addUser(userRequest)
                .subscribe(new Observer<UserResponseBody>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {
                        // Handle subscription
                        Log.e("TAG", "onSubscribe: "+ d );
                    }

                    @Override
                    public void onNext(@io.reactivex.rxjava3.annotations.NonNull UserResponseBody userResponseBody) {
                        Log.e("TAG", "onNext: "+ userResponseBody );
                        _user.postValue(userResponseBody);
                    }



                    @Override
                    public void onError(@NonNull Throwable e) {
                        // Handle errors
                        Log.e("TAG", "onError: "+ e );
                    }

                    @Override
                    public void onComplete() {
                        // Handle completion
                        Log.e("TAG", "onComplete: " );
                    }
                });
    }

}
