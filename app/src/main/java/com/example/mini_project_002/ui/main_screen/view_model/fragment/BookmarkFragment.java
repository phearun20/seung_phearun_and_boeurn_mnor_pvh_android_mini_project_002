package com.example.mini_project_002.ui.main_screen.view_model.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mini_project_002.R;
import com.example.mini_project_002.data.adapter.BookmarkAdapter;
import com.example.mini_project_002.data.model.model.Bookmark;
import com.example.mini_project_002.data.model.request.BookMarkRequest;
import com.example.mini_project_002.data.model.response.BookmarkResponse;
import com.example.mini_project_002.databinding.FragmentBookmarkBinding;
import com.example.mini_project_002.ui.main_screen.view_model.BookmarkViewModel;

import java.util.ArrayList;
import java.util.List;

public class BookmarkFragment extends Fragment {
    BookmarkViewModel bookmarkViewModel;
    private ArrayList<Bookmark> bookmarks;
    BookmarkResponse bookmarkResponse;
    RecyclerView recyclerView;
    private BookmarkAdapter bookmarkAdapter;
    BookMarkRequest bookMarkRequest;
    View view;
//    String id = "32535d58-e5c5-4766-a1fb-146f73531429";
    FragmentBookmarkBinding binding;
    SharedPreferences sharedPreferences;
    String userId;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bookmark, container, false);
        view = binding.getRoot();
        bookmarkViewModel = new ViewModelProvider(requireActivity(), new ViewModelProvider.AndroidViewModelFactory(getActivity().getApplication())).get(BookmarkViewModel.class);
        sharedPreferences = getActivity().getSharedPreferences("User", Context.MODE_PRIVATE);
        userId = sharedPreferences.getString("UserId", "");
        Log.d("TAG", "onCreate: userId"+userId);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        bookmarkViewModel.getAllBookmarkByUserId(userId);


        bookmarkViewModel.bookmarkLiveData.observe(getViewLifecycleOwner(), new Observer<List<Bookmark>>() {
            @Override
            public void onChanged(List<Bookmark> bookmarkList) {
//                Log.d("TAG", "onChanged: response" + bookmarkList.get(0).getTeacher().getId());
                bookmarks = new ArrayList<>();
                bookmarks.addAll(bookmarkList);
                bookmarkAdapter = new BookmarkAdapter(getContext(), bookmarks);
                binding.bookmarkRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                binding.bookmarkRecyclerView.setAdapter(bookmarkAdapter);
            }
        });


//        bookmarkViewModel.bookmarkLiveData.observe(getViewLifecycleOwner(), new Observer<BookmarkResponse>() {
//            @Override
//            public void onChanged(BookmarkResponse bookmarkResponse) {
//                Log.d("TAG", "onChanged: response" + bookmarkResponse.getPayload().get(0).getArticleId());
//                bookmarks = new ArrayList<>();
//                bookmarks.addAll(bookmarkResponse.getPayload());
//                bookmarkAdapter = new BookmarkAdapter(getContext(), bookmarks);
//                binding.bookmarkRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//                binding.bookmarkRecyclerView.setAdapter(bookmarkAdapter);
//            }
//        });
    }
}