package com.example.mini_project_002.data.service;

import com.example.mini_project_002.data.model.UserResponseBody;
import com.example.mini_project_002.data.model.request.UserRequest;
import com.example.mini_project_002.data.model.response.UserResponse;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface UserService {
    @POST("api/v1/users")
    Observable<UserResponseBody> addNewUser(@Body UserRequest userRequest);
    @PUT("api/v1/users/{userId}")
    Observable<UserResponseBody> updateUser(@Body UserRequest userRequest, @Path("userId") String userId);
    @GET("api/v1/users/{id}")
    Observable<UserResponseBody>  getOwnerUser(@Path("id") String id);

}
