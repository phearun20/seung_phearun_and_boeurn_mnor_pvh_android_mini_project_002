package com.example.mini_project_002.data.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.mini_project_002.R;
import com.example.mini_project_002.data.model.model.Article;
import com.example.mini_project_002.databinding.CustomCardPostLayoutBinding;

import java.util.List;

public class OunerArticleAdapter extends RecyclerView.Adapter<OunerArticleAdapter.ArticleViewHolder> {
    static CustomCardPostLayoutBinding binding;
    Context context;
    List<Article> articleList;

    public OunerArticleAdapter(Context context, List<Article> articles) {
        this.context = context;
        this.articleList = articles;
    }

    @NonNull
    @Override
    public ArticleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = CustomCardPostLayoutBinding.inflate(LayoutInflater.from(parent.getContext()));
        return new ArticleViewHolder(binding.getRoot());
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull ArticleViewHolder holder, int position) {
        Article article = articleList.get(position);
        holder.bind(article, holder.itemView.getContext());
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    public class ArticleViewHolder extends RecyclerView.ViewHolder{
        public ArticleViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public void bind(Article article, Context context) {
            try {
                binding.txtUsername.setText(article.getTeacher().getUsername());
                binding.txtDescription.setText(article.getDescription());

                if (article.getIsPublished().equals("true")){
                    binding.txtStatus.setText("Public");
                }else {
                    binding.txtStatus.setText("Private");
                }

                Glide.with(context)
                        .load(article.getThumbnail())
                        .placeholder(R.drawable.pnhom_penh_pic)
                        .into(binding.ivPicture);

                Glide.with(context)
                        .load(article.getTeacher().getProfile())
                        .placeholder(R.drawable.profile2_post)
                        .into(binding.ivProfile);

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
