package com.example.mini_project_002.data.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mini_project_002.data.model.model.Article;
import com.example.mini_project_002.databinding.CustomCardPostLayoutBinding;

import java.util.List;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ProfileViewHolder> {
    static CustomCardPostLayoutBinding binding;
    Context context;
    List<Article> articleList;

    public ProfileAdapter(Context context, List<Article> articleList) {
        this.context = context;
        this.articleList = articleList;
    }

    @NonNull
    @Override
    public ProfileViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = CustomCardPostLayoutBinding.inflate(LayoutInflater.from(parent.getContext()));
        return new ProfileViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileViewHolder holder, int position) {
        Article article = articleList.get(position);
        holder.bind(article,holder.itemView.getContext());
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ProfileViewHolder extends RecyclerView.ViewHolder {

        public ProfileViewHolder(@NonNull View itemView) {
            super(itemView);

        }

        public void bind(Article article, Context context) {

        }
    }
}
