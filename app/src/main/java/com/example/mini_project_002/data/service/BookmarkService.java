package com.example.mini_project_002.data.service;

import com.example.mini_project_002.data.model.request.BookMarkRequest;
import com.example.mini_project_002.data.model.response.BookmarkResponse;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface BookmarkService {
    @GET("/api/v1/bookmarks/user/{id}")
    Observable<BookmarkResponse> getAllBookmark(@Path("id") String id);
    @POST("/api/v1/bookmarks/user/{id}")
    Observable<Void> addBookmark(@Path("id") String id,@Body BookMarkRequest bookMarkRequest);

}
