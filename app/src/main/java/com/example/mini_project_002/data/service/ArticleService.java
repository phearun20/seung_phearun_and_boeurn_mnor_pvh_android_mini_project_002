package com.example.mini_project_002.data.service;

import com.example.mini_project_002.data.model.model.Article;
import com.example.mini_project_002.data.model.request.ArticleRequest;
import com.example.mini_project_002.data.model.response.ArticleResponse;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ArticleService {

    @GET("/api/v1/articles")
    Observable<ArticleResponse> getAllArticles(@Query("page") String page, @Query("size") String size);
    @POST("/api/v1/articles")
    Observable<Void> addArticle(@Body ArticleRequest articleRequest);
}
