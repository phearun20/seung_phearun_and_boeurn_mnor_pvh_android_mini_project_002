package com.example.mini_project_002.data.model.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Article {
    @SerializedName("id")
    String id;
    @SerializedName("title")
    String title;
    @SerializedName("description")
    String description;
    @SerializedName("isPublished")
    Boolean isPublished;
    @SerializedName("categories")
    List<Category> categories;
    @SerializedName("teacher")
    private User teacher;
    @SerializedName("comments")
    List<String> comments;
    @SerializedName("thumbnail")
    String thumbnail;
    @SerializedName("createdDate")
    String createdDate;
    @SerializedName("lastModified")
    String lastModified;

    public Article(String id, String title, String description, Boolean isPublished, List<Category> categories, User teacher, List<String> comments, String thumbnail, String createdDate, String lastModified) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.isPublished = isPublished;
        this.categories = categories;
        this.teacher = teacher;
        this.comments = comments;
        this.thumbnail = thumbnail;
        this.createdDate = createdDate;
        this.lastModified = lastModified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getIsPublished() {
        return isPublished;
    }

    public void setIsPublished(Boolean isPublished) {
        this.isPublished = isPublished;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public User getTeacher() {
        return teacher;
    }

    public void setTeacher(User teacher) {
        this.teacher = teacher;
    }

    public List<String> getComments() {
        return comments;
    }

    public void setComments(List<String> comments) {
        this.comments = comments;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLastModified() {
        return lastModified;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", isPublished=" + isPublished +
                ", categories=" + categories +
                ", teacher=" + teacher +
                ", comments=" + comments +
                ", thumbnail='" + thumbnail + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", lastModified='" + lastModified + '\'' +
                '}';
    }
}
