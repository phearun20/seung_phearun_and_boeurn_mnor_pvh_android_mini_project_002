package com.example.mini_project_002.data.model.request;

import com.google.gson.annotations.SerializedName;

public class BookMarkRequest {
    @SerializedName("articleId")
    private String articleId;

    public BookMarkRequest(String articleId) {
        this.articleId = articleId;
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    @Override
    public String toString() {
        return "BookMark{" +
                "articleId='" + articleId + '\'' +
                '}';
    }
}
