package com.example.mini_project_002.data.model.response;

import com.example.mini_project_002.data.model.model.Article;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleResponse {

    @SerializedName("message")
    String message;
    @SerializedName("status")
    String status;
    @SerializedName("payload")
    private List<Article> payload;

    public ArticleResponse(String message, String status, List<Article> payload) {
        this.message = message;
        this.status = status;
        this.payload = payload;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Article> getPayload() {
        return payload;
    }

    public void setPayload(List<Article> payload) {
        this.payload = payload;
    }
}
