package com.example.mini_project_002.data.repo;

import com.example.mini_project_002.APIClient;
import com.example.mini_project_002.data.model.model.Article;
import com.example.mini_project_002.data.model.request.ArticleRequest;
import com.example.mini_project_002.data.model.response.ArticleResponse;
import com.example.mini_project_002.data.service.ArticleService;

import io.reactivex.rxjava3.core.Observable;

public class ArticleRepository {

    ArticleService articleService;

    public ArticleRepository() {
        articleService = APIClient.getRetrofitInstance().create(ArticleService.class);
    }

    public Observable<ArticleResponse> getAllArticles(String page, String size) {
        return articleService.getAllArticles(page, size);
    }
    public Observable<Void> addArticle(ArticleRequest articleRequest){
        return articleService.addArticle(articleRequest);
    }
}
