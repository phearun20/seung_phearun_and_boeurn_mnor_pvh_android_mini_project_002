package com.example.mini_project_002.data.model.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Bookmark {
    @SerializedName("articleId")
    String articleId;
    @SerializedName("title")
    String title;
    @SerializedName("thumbnail")
    String thumbnail;
    @SerializedName("description")
    String description;
    @SerializedName("teacher")
    User teacher;

    public Bookmark(String articleId, String title, String thumbnail, String description, User teacher) {
        this.articleId = articleId;
        this.title = title;
        this.thumbnail = thumbnail;
        this.description = description;
        this.teacher = teacher;
    }

    @Override
    public String toString() {
        return "Bookmark{" +
                "articleId='" + articleId + '\'' +
                ", title='" + title + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", description='" + description + '\'' +
                ", teacher=" + teacher +
                '}';
    }

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getTeacher() {
        return teacher;
    }

    public void setTeacher(User teacher) {
        this.teacher = teacher;
    }
}
