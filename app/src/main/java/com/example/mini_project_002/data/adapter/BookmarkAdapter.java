package com.example.mini_project_002.data.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.mini_project_002.R;
import com.example.mini_project_002.data.model.model.Article;
import com.example.mini_project_002.data.model.model.Bookmark;
import com.example.mini_project_002.databinding.CustomCardBookmarkBinding;

import java.util.List;

public class BookmarkAdapter extends RecyclerView.Adapter<BookmarkAdapter.BookmarkViewHolder>{
    static CustomCardBookmarkBinding binding;
    Context context;
    List<Bookmark> bookmarkList;

    public BookmarkAdapter(Context context, List<Bookmark> bookmarkList) {
        this.context = context;
        this.bookmarkList = bookmarkList;
    }

    @NonNull
    @Override
    public BookmarkViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        binding = CustomCardBookmarkBinding.inflate(LayoutInflater.from(parent.getContext()));
        return new BookmarkViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull BookmarkViewHolder holder, int position) {
        Bookmark bookmark = bookmarkList.get(position);
        holder.bind(bookmark, holder.itemView.getContext());
    }

    @Override
    public int getItemCount() {
        return bookmarkList.size();
    }

    public class BookmarkViewHolder extends RecyclerView.ViewHolder{

        public BookmarkViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        public void bind(Bookmark bookmark, Context context) {
            try {
                binding.txtUsername.setText(bookmark.getTeacher().getUsername());
                binding.txtDescription.setText(bookmark.getDescription());

                Glide.with(context)
                        .load(bookmark.getThumbnail())
                        .placeholder(R.drawable.beautiful_view)
                        .into(binding.ivPicture);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
