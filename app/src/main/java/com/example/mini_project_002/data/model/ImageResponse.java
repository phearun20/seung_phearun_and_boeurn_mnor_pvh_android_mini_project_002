package com.example.mini_project_002.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ImageResponse {
    @SerializedName("message")
    String message;
    @SerializedName("status")
    String status;
    @SerializedName("payload")
    List<String> imageurl;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public List<String> getImageurl() {
        return imageurl;
    }


}
