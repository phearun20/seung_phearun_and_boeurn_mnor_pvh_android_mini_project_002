package com.example.mini_project_002.data.model.response;

import com.example.mini_project_002.data.model.model.Category;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CategoryResponse {
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;
    @SerializedName("payload")
    private List<Category> payload;
    @SerializedName("page")
    private Integer page;
    @SerializedName("size")
    private Integer size;
    @SerializedName("totalElements")
    private Integer totalElements;
    @SerializedName("totalPages")
    private Integer totalPages;

    @Override
    public String toString() {
        return "CategoryResponse{" +
                "message='" + message + '\'' +
                ", status='" + status + '\'' +
                ", payload=" + payload +
                ", page=" + page +
                ", size=" + size +
                ", totalElements=" + totalElements +
                ", totalPages=" + totalPages +
                '}';
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Category> getPayload() {
        return payload;
    }

    public void setPayload(List<Category> payload) {
        this.payload = payload;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }

    public Integer getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }
}
