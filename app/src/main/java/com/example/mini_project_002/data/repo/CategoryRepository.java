package com.example.mini_project_002.data.repo;

import com.example.mini_project_002.APIClient;
import com.example.mini_project_002.data.model.UserResponseBody;
import com.example.mini_project_002.data.model.request.UserRequest;
import com.example.mini_project_002.data.model.response.CategoryResponse;
import com.example.mini_project_002.data.service.CategoryService;
import com.example.mini_project_002.data.service.UserService;

import io.reactivex.rxjava3.core.Observable;

public class CategoryRepository  {
    CategoryService service;

    public CategoryRepository() {
        service = APIClient.getRetrofitInstance().create(CategoryService.class);
    }
    public Observable<CategoryResponse> getAllUser(String page,String size) {
        return service.getAllCategory(page,size);
    }
}
