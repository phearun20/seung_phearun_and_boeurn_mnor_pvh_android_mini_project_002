package com.example.mini_project_002.data.service;

import com.example.mini_project_002.data.model.ImageResponse;

import io.reactivex.rxjava3.core.Observable;
import okhttp3.MultipartBody;

import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ImageService {
    @Multipart
    @POST("api/v1/files")
    Observable<ImageResponse> uploadImage(@Part MultipartBody.Part[] image);
}
