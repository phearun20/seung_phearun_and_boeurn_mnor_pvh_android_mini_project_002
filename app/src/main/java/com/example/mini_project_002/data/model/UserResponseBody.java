package com.example.mini_project_002.data.model;

import com.example.mini_project_002.data.model.response.UserResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserResponseBody {
    @SerializedName("message")
    private String message;
    @SerializedName("status")
    private String status;
    @SerializedName("payload")
    private UserResponse payload;

    public String getMessage() {
        return message;
    }

    public UserResponse getPayload() {
        return payload;
    }

    public void setPayload(UserResponse payload) {
        this.payload = payload;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BaseResponse{" +
                "message='" + message + '\'' +
                ", status='" + status + '\'' +
                ", payload=" + payload +
                '}';
    }
}
