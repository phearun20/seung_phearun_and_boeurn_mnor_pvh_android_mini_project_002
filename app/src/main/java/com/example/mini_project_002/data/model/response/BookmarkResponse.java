package com.example.mini_project_002.data.model.response;

import com.example.mini_project_002.data.model.model.Article;
import com.example.mini_project_002.data.model.model.Bookmark;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BookmarkResponse {
    @SerializedName("message")
    String message;
    @SerializedName("status")
    String status;
    @SerializedName("payload")
    List<Bookmark> payload;
    @SerializedName("page")
    String page;
    @SerializedName("size")
    String size;
    @SerializedName("totalElements")
    String totalElements;
    @SerializedName("totalPages")
    String totalPages;

    public BookmarkResponse(String message, String status, List<Bookmark> payload, String page, String size, String totalElements, String totalPages) {
        this.message = message;
        this.status = status;
        this.payload = payload;
        this.page = page;
        this.size = size;
        this.totalElements = totalElements;
        this.totalPages = totalPages;
    }

    @Override
    public String toString() {
        return "BookmarkResponse{" +
                "message='" + message + '\'' +
                ", status='" + status + '\'' +
                ", payload=" + payload +
                ", page='" + page + '\'' +
                ", size='" + size + '\'' +
                ", totalElements='" + totalElements + '\'' +
                ", totalPages='" + totalPages + '\'' +
                '}';
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Bookmark> getPayload() {
        return payload;
    }

    public void setPayload(List<Bookmark> payload) {
        this.payload = payload;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(String totalElements) {
        this.totalElements = totalElements;
    }

    public String getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(String totalPages) {
        this.totalPages = totalPages;
    }
}
