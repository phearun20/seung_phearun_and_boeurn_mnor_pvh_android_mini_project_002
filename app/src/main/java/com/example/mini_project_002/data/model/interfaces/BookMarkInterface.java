package com.example.mini_project_002.data.model.interfaces;

public interface BookMarkInterface {
    public void onClickItem(int position);
}
