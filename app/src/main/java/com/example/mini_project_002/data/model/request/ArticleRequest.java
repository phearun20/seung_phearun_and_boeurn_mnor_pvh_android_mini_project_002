package com.example.mini_project_002.data.model.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleRequest {
        @SerializedName("title")
        private String title;
        @SerializedName("description")
        private String description;
        @SerializedName("categories")
        private List<String> categories;
        @SerializedName("teacherId")
        private String teacherId;
        @SerializedName("isPublished")
        private Boolean isPublished;
        @SerializedName("thumbnail")
        private String thumbnail;



        public ArticleRequest(String title, String description, List<String> categories, String teacherId, Boolean isPublished, String thumbnail) {
                this.title = title;
                this.description = description;
                this.categories = categories;
                this.teacherId = teacherId;
                this.isPublished = isPublished;
                this.thumbnail = thumbnail;
        }

        public String getTitle() {
                return title;
        }

        public void setTitle(String title) {
                this.title = title;
        }

        public String getDescription() {
                return description;
        }

        public void setDescription(String description) {
                this.description = description;
        }

        public List<String> getCategories() {
                return categories;
        }

        public void setCategories(List<String> categories) {
                this.categories = categories;
        }

        public String getTeacherId() {
                return teacherId;
        }

        public void setTeacherId(String teacherId) {
                this.teacherId = teacherId;
        }

        public Boolean getPublished() {
                return isPublished;
        }

        public void setPublished(Boolean published) {
                isPublished = published;
        }

        public String getThumbnail() {
                return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
                this.thumbnail = thumbnail;
        }

        @Override
        public String toString() {
                return "ArticleRequest{" +
                        "title='" + title + '\'' +
                        ", description='" + description + '\'' +
                        ", categories=" + categories +
                        ", teacherId='" + teacherId + '\'' +
                        ", isPublished=" + isPublished +
                        ", thumbnail='" + thumbnail + '\'' +
                        '}';
        }
}
