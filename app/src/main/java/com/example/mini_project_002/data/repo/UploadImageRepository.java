package com.example.mini_project_002.data.repo;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.mini_project_002.APIClient;
import com.example.mini_project_002.data.model.ImageResponse;
import com.example.mini_project_002.data.service.ImageService;

import java.io.File;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UploadImageRepository {
    ImageService imageService;

    public UploadImageRepository() {
        imageService = APIClient.getRetrofitInstance().create(ImageService.class);
    }

    public Observable<ImageResponse> uploadImage(MultipartBody.Part[] file) {
        return imageService.uploadImage(file);
    }
 MutableLiveData<ImageResponse> imageLiveData = new MutableLiveData<>();
    public LiveData<ImageResponse> user = imageLiveData;

}
