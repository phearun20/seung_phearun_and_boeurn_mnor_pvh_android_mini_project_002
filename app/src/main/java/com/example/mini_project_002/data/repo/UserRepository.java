package com.example.mini_project_002.data.repo;

import com.example.mini_project_002.APIClient;
import com.example.mini_project_002.data.model.UserResponseBody;
import com.example.mini_project_002.data.model.request.UserRequest;
import com.example.mini_project_002.data.model.response.UserResponse;
import com.example.mini_project_002.data.service.UserService;

import io.reactivex.rxjava3.core.Observable;


public class UserRepository {
UserService userService;

    public UserRepository() {
        userService =APIClient.getRetrofitInstance().create(UserService.class);
    }

    public Observable<UserResponseBody> addUser(UserRequest userRequest) {
        return userService.addNewUser(userRequest);
    }
    public Observable<UserResponseBody> updateUser(UserRequest userRequest,String userId){
        return userService.updateUser(userRequest,userId);
    }

    public Observable<UserResponseBody> getOwnerUser( String id) {
        return userService.getOwnerUser(id);
    }

}
