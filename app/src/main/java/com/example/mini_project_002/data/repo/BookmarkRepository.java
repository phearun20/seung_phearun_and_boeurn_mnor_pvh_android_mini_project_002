package com.example.mini_project_002.data.repo;

import com.example.mini_project_002.APIClient;
import com.example.mini_project_002.data.model.request.BookMarkRequest;
import com.example.mini_project_002.data.model.response.BookmarkResponse;
import com.example.mini_project_002.data.service.BookmarkService;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.Response;

public class BookmarkRepository {

    BookmarkService bookmarkService;

    public BookmarkRepository() {
        bookmarkService = APIClient.getRetrofitInstance().create(BookmarkService.class);
    }
    public Observable<BookmarkResponse> getAllBookmarkByUserId(String id){
        return bookmarkService.getAllBookmark(id);
    }
    public Observable<Void> addBookMark(String id, BookMarkRequest bookMarkRequest){
        return bookmarkService.addBookmark(id,bookMarkRequest);
    }

}
