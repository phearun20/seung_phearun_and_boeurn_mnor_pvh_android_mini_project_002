package com.example.mini_project_002.data.service;

import com.example.mini_project_002.data.model.response.CategoryResponse;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface CategoryService {
    @GET("api/v1/categories")
    Observable<CategoryResponse> getAllCategory(@Query("page") String page, @Query("size") String size);

}
